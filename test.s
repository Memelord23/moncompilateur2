			# This code was produced by the CERI Compiler
.data
FormatString1:	.string "%llu"	# used by printf to display 64-bit unsigned integers
FormatString2:	.string "%lf"	# used by printf to display 64-bit floating point numbers
FormatString3:	.string "%c"	# used by printf to display a 8-bit single character
TrueString:	.string "TRUE"	# used by printf to display the boolean value TRUE
FalseString:	.string "FALSE"	# used by printf to display the boolean value FALSE
a:	.quad 0
b:	.quad 0
c:	.quad 0
c1:	.byte 0
c2:	.byte 0
denum:	.double 0.0
frac:	.double 0.0
num:	.double 0.0
	.align 8
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	movq $0, %rax
	movb $'f',%al
	push %rax	# push a 64-bit version of 'f'
	pop %rax
	movb %al,c1
	movq $0, %rax
	movb $'a',%al
	push %rax	# push a 64-bit version of 'a'
	pop %rax
	movb %al,c2
	subq $8,%rsp			# allocate 8 bytes on stack's top
	movl	$0, (%rsp)	# Conversion of 1 (32 bit high part)
	movl	$1072693248, 4(%rsp)	# Conversion of 1 (32 bit low part)
	pop num
	subq $8,%rsp			# allocate 8 bytes on stack's top
	movl	$0, (%rsp)	# Conversion of 1 (32 bit high part)
	movl	$1072693248, 4(%rsp)	# Conversion of 1 (32 bit low part)
	pop denum
	push num
	push denum
	fldl	(%rsp)	
	fldl	8(%rsp)	# first operand -> %st(0) ; second operand -> %st(1)
	fdivp	%st(0),%st(1)	# %st(0) <- op1 + op2 ; %st(1)=null
	fstpl 8(%rsp)
	addq	$8, %rsp	# result on stack's top
	pop frac
	push $1
	pop a
For0:
debutfor0:			# debut de for:
	push $0
	pop c
testfor0:			# expresion a testé:
	push c
	push $5
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	ja Vrai2	# If above
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	pop %rax
	cmpq $0, %rax
	jne Finfor0 		
	addq $1, c	
	push c
	pop %rsi	# The value to be displayed
	movq $FormatString1, %rdi	# "%llu\n"
	movl	$0, %eax
	call	printf@PLT
	jmp testfor0	 #retour a la comparaison
Finfor0:			# fin du for:
	push $2
	pop c
Case4:
switch4:			# debut de switch:
	push $1
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	push c
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jne Suite5	# If not equal go to next case
	push c1
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push $0		# False
	jmp Finswitch4
Vrai5:	push $0xFFFFFFFFFFFFFFFF		# True
Suite5:
	push $1
	push $2
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	push c
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jne Suite7	# If not equal go to next case
	push c2
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push $0		# False
	jmp Finswitch4
Vrai7:	push $0xFFFFFFFFFFFFFFFF		# True
Suite7:
	push $1
	push $3
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	push c
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jne Suite9	# If not equal go to next case
	push num
	movsd	(%rsp), %xmm0		# &stack top -> %xmm0
	subq	$16, %rsp		# allocation for 3 additional doubles
	movsd %xmm0, 8(%rsp)
	movq $FormatString2, %rdi	# "%lf\n"
	movq	$1, %rax
	call	printf
nop
	addq $24, %rsp			# pop nothing
	push $0		# False
	jmp Finswitch4
Vrai9:	push $0xFFFFFFFFFFFFFFFF		# True
Suite9:
	push a
	pop %rsi	# The value to be displayed
	movq $FormatString1, %rdi	# "%llu\n"
	movl	$0, %eax
	call	printf@PLT
Finswitch4:			# fin du switch:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
